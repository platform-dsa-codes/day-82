//{ Driver Code Starts
import java.util.Scanner;

class GfG
{
	public static void main(String args[])
	{
		 Scanner sc = new Scanner(System.in);
		 int t=sc.nextInt();
		 while(t>0)
		 {
			MyQueue obj = new MyQueue();
			int Q = sc.nextInt();
			while(Q>0)
			{
				int QueryType = 0;
				QueryType = sc.nextInt();
				if(QueryType == 1)
				{
					int a = sc.nextInt();
					
					obj.push(a);
					
				}else if(QueryType == 2)
				{
				System.out.print(obj.pop()+" ");
				}
				Q--;
			}
			System.out.println("");
			t--;
		 }
	}
}




// } Driver Code Ends


class MyQueue {
    int front, rear;
    int arr[] = new int[100005];

    MyQueue() {
        front = 0;
        rear = 0;
    }

    // Function to push an element x into the queue.
    void push(int x) {
        // Increment rear and add the element at the rear index
        arr[rear++] = x;
    }

    // Function to pop an element from the queue and return that element.
    int pop() {
        // If front is equal to rear, the queue is empty
        if (front == rear) {
            return -1;
        }
        // Return the element at the front index and then increment front
        return arr[front++];
    }
}
